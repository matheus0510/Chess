package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Rainha;
import model.Rei;
import model.Square;
import model.Torre;
import control.SquareControl;
import model.Peca;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color colorMove = Color.CYAN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, colorMove);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		String peacePath = "icon/Black P_48x48.png";
		Peca umPeao = new Peao(peacePath, "Peao");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPeca(umPeao);
		}
		peacePath = "icon/Black R_48x48.png";
		Peca umaTorre = new Torre(peacePath, "Torre");
		this.squareControl.getSquare(0, 0).setPeca(umaTorre);
		this.squareControl.getSquare(0, 7).setPeca(umaTorre);

		peacePath = "icon/Black N_48x48.png";
		Peca umCavalo = new Cavalo(peacePath,"Cavalo");
		this.squareControl.getSquare(0, 1).setPeca(umCavalo);
		this.squareControl.getSquare(0, 6).setPeca(umCavalo);

		peacePath = "icon/Black B_48x48.png";
		Peca umBispo = new Bispo(peacePath,"Bispo");
		this.squareControl.getSquare(0, 2).setPeca(umBispo);
		this.squareControl.getSquare(0, 5).setPeca(umBispo);

		peacePath = "icon/Black Q_48x48.png";
		Peca umaRainha = new Rainha(peacePath, "Rainha"); 
		this.squareControl.getSquare(0, 4).setPeca(umaRainha);

		peacePath = "icon/Black K_48x48.png";
		Peca umRei = new Rei(peacePath, "Rei"); 
		this.squareControl.getSquare(0, 3).setPeca(umRei);

		
		peacePath = "icon/White P_48x48.png";
		Peca umPeaoBranco = new Peao(peacePath, "Peao");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPeca(umPeaoBranco);
		}
		
		peacePath = "icon/White R_48x48.png";
		Peca umaTorreBranca = new Torre(peacePath, "Torre");
		this.squareControl.getSquare(7, 0).setPeca(umaTorreBranca);
		this.squareControl.getSquare(7, 7).setPeca(umaTorreBranca);

		peacePath = "icon/White N_48x48.png";
		Peca umCavaloBranco = new Cavalo(peacePath,"Cavalo");
		this.squareControl.getSquare(7, 1).setPeca(umCavaloBranco);
		this.squareControl.getSquare(7, 6).setPeca(umCavaloBranco);

		peacePath = "icon/White B_48x48.png";
		Peca umBispoBranco = new Bispo(peacePath,"Bispo");
		this.squareControl.getSquare(7, 2).setPeca(umBispoBranco);
		this.squareControl.getSquare(7, 5).setPeca(umBispoBranco);

		peacePath = "icon/White Q_48x48.png";
		Peca umaRainhaBranca = new Rainha(peacePath, "Rainha"); 
		this.squareControl.getSquare(7, 4).setPeca(umaRainhaBranca);

		peacePath = "icon/White K_48x48.png";
		Peca umReiBranco = new Rei(peacePath, "Rei"); 
		this.squareControl.getSquare(7, 3).setPeca(umReiBranco);
	}
}
