package control;

import java.awt.Color;
import java.util.ArrayList;
import java.awt.Point;

import model.Peca;
import model.Torre;
import model.Square;
import model.Square.SquareEventListener;
import model.outOfPossibleSquaresException;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_MOVE = Color.CYAN;

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorMove;

	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private ArrayList<Square> possibleSquares;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED,  DEFAULT_COLOR_MOVE);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorMove) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorMove = colorMove;

		this.squareList = new ArrayList<>();
		this.possibleSquares = new ArrayList<>();
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		System.out.println("x=" + square.getPosition().x + " y=" + square.getPosition().y);
		square.setColor(this.colorHover);
	}

	@Override
	public void onSelectEvent(Square square) throws outOfPossibleSquaresException{
		if (haveSelectedCellPanel()) {
			if(!possibleSquares.contains(square) && !this.selectedSquare.equals(square))
			{
				throw new outOfPossibleSquaresException("Movimentação não diponível!");
			}
			if (!this.selectedSquare.equals(square)) {
				moveContentOfSelectedSquare(square);
			} else {
				unselectSquare(square);
			}
		} else {
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {		
		if (this.selectedSquare != square && !possibleSquares.contains(square) ) {	
			resetColor(square);
		}else {
			if(this.selectedSquare == square)
				square.setColor(this.colorSelected);		
			else
				square.setColor(Color.CYAN);
		}		
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setPeca(this.selectedSquare.getPeca());
		this.selectedSquare.removePeca();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.haveImagePath()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showPossibleMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		for(int i = 0; i<possibleSquares.size(); i++){
			resetColor(possibleSquares.get(i));
		}
		possibleSquares.clear();
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
	private void showPossibleMovesPeaoB(Square square) {
		ArrayList<Point> movesPeao = square.getPeca().getMoveB(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i <movesPeao.size(); i++) {
			Square squares = getSquare(movesPeao.get(i).x, movesPeao.get(i).y);
			Square squareEsquerdo = getSquare(square.getPosition().x+1, square.getPosition().y+1);
			Square squareDireito = getSquare(square.getPosition().x+1, square.getPosition().y-1);
			Square squareF = getSquare(square.getPosition().x+1, square.getPosition().y);

			if( ( square.getPosition().y != 0 && squares == squareDireito && squareDireito.haveImagePath() )   
					||	(square.getPosition().y != 7 && squares == squareEsquerdo && squareEsquerdo.haveImagePath() ) ) {

				if(squares.getPeca().getImagePath().contains("White")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareDireito && squares != squareEsquerdo) {		

				if(!squares.haveImagePath() && !squareF.haveImagePath()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}	
			}
		}		
	}
	
	private void showPossibleMovesPeaoW(Square square) {
		ArrayList<Point> movesPeao = square.getPeca().getMoveW(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i <movesPeao.size(); i++) {
			
			Square squares = getSquare(movesPeao.get(i).x, movesPeao.get(i).y);
			Square squareEsquerdo = getSquare(square.getPosition().x-1, square.getPosition().y-1);
			Square squareDireito = getSquare(square.getPosition().x-1, square.getPosition().y+1);
			Square squareF = getSquare(square.getPosition().x-1, square.getPosition().y);
			if( ( square.getPosition().y != 7 && squares == squareDireito && squareDireito.haveImagePath() )   
					||	(square.getPosition().y != 0 && squares == squareEsquerdo && squareEsquerdo.haveImagePath() ) ) {

				if(squares.getPeca().getImagePath().contains("Black")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareDireito && squares != squareEsquerdo) {		

				if(!squares.haveImagePath() && !squareF.haveImagePath()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}	
			}						
		}		
	}
	
	private void showPossibleMovesRainha(Square square) {		

		ArrayList<Point> movesRainha = square.getPeca().getMove(square.getPosition().x, square.getPosition().y);


		int i = 0;
		Square squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);		
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK DIREITA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA DIREITA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}
		System.out.println("I = " + i);
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK ESQUERDA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}	

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK BAIXO " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA BAIXO " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK CIMA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA CIMA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}
		
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);	

		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK DIREITA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);		
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA DIREITA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}	

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK ESQUERDA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK ESQUERDA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {		

			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK DA ESQUERDA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}
	}
	
	private void showPossibleMovesRei(Square square) {
		ArrayList<Point> movesRei = square.getPeca().getMove(square.getPosition().x, square.getPosition().y);
		for (int i = 0; i <movesRei.size(); i++) {
			
			Square squares = getSquare(movesRei.get(i).x, movesRei.get(i).y);
			if(square.getPeca().getImagePath().contains("White")) {
				if(!squares.haveImagePath()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPeca().getImagePath().contains("Black")) {
				if(!squares.haveImagePath()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}		
			}				
		}
	}
	
	private void showPossibleMovesBispo(Square square) {

		ArrayList<Point> movesBispo = square.getPeca().getMove(square.getPosition().x, square.getPosition().y);
		
		int i = 0;
		Square squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);	
		
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {		
			
			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK DIREITA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);		
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA DIREITA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}	
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
			
			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK ESQUERDA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
			
			if(squares.haveImagePath()){
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK ESQUERDA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
				}
				break;
			}else{
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {		
			
			if(squares.haveImagePath())
			{
				System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK ESQUERDA " + squares.getPosition().y);
				if(square.getPeca().getImagePath().contains("White")) {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPeca().getImagePath().contains("Black")) {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}	
	}
	
private void showPossibleMovesCavalo(Square square) {
		
		ArrayList<Point> movesCavalo = square.getPeca().getMove(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i <movesCavalo.size(); i++) {			
			Square squares = getSquare(movesCavalo.get(i).x, movesCavalo.get(i).y);							
			
			if(square.getPeca().getImagePath().contains("White")) {
				if(!squares.haveImagePath()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPeca().getImagePath().contains("Black")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPeca().getImagePath().contains("Black")) {
				if(!squares.haveImagePath()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPeca().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}		
			}			
		}		
	}

private void showPossibleMovesTorre(Square square) {
	
	Torre torre = (Torre) square.getPeca();
	
	ArrayList<Point> movesTorre = torre.getMove(square.getPosition().x, square.getPosition().y);			
	
	int i = 0;
	Square squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);		
	while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {		
		
		if(squares.haveImagePath())	{
			System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK DIREITA " + squares.getPosition().y);
			if(square.getPeca().getImagePath().contains("White")) {
				if(squares.getPeca().getImagePath().contains("Black")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}	
			if(square.getPeca().getImagePath().contains("Black")) {
				if(squares.getPeca().getImagePath().contains("White")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);	
				}	
			}
			while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
				i++;
				squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
			}
			break;
		}
		else {
			System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA DIREITA " + squares.getPosition().y);
			squares.setColor(this.colorMove);
			possibleSquares.add(squares);
		}
		i++;
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
	}
	System.out.println("I = " + i);
	squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
	while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {		
		
		if(squares.haveImagePath())
		{
			System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK ESQUERDA " + squares.getPosition().y);
			if(square.getPeca().getImagePath().contains("White")) {
				if(squares.getPeca().getImagePath().contains("Black")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}	
			if(square.getPeca().getImagePath().contains("Black")) {
				if(squares.getPeca().getImagePath().contains("White")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);	
				}	
			}
			while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
				i++;
				squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
			}
			break;
		}
		else {
			System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA ESQUERDA " + squares.getPosition().y);
			squares.setColor(this.colorMove);
			possibleSquares.add(squares);
		}
		i++;
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
	}	
	
	squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
	while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {		
		
		if(squares.haveImagePath())
		{
			System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK BAIXO " + squares.getPosition().y);
			if(square.getPeca().getImagePath().contains("White")) {
				if(squares.getPeca().getImagePath().contains("Black")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}	
			if(square.getPeca().getImagePath().contains("Black")) {
				if(squares.getPeca().getImagePath().contains("White")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);	
				}	
			}
			while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
				i++;
				squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
			}
			break;
		}
		else {
			System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA BAIXO " + squares.getPosition().y);
			squares.setColor(this.colorMove);
			possibleSquares.add(squares);
		}
		i++;
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
	}
	
	squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
	while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {		
		
		if(squares.haveImagePath())
		{
			System.out.println(squares.getPosition().x + " POSIÇÃO DO BREAK CIMA " + squares.getPosition().y);
			if(square.getPeca().getImagePath().contains("White")) {
				if(squares.getPeca().getImagePath().contains("Black")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}	
			if(square.getPeca().getImagePath().contains("Black")) {
				if(squares.getPeca().getImagePath().contains("White")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);	
				}	
			}
			break;
		}else {
			System.out.println(squares.getPosition().x + " POSIÇÃO QUE PINTA CIMA " + squares.getPosition().y);
			squares.setColor(this.colorMove);
			possibleSquares.add(squares);
		}
		i++;
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
	}			
}

	private void showPossibleMoves(Square square){
		Peca peca = square.getPeca();
		
		if(peca.getTypeOfObject().equalsIgnoreCase("Peao")){
			if(peca.getImagePath().contains("White"))
				showPossibleMovesPeaoW(square);
			if(peca.getImagePath().contains("Black"))
				showPossibleMovesPeaoB(square);
		}
		
		if(peca.getTypeOfObject().equalsIgnoreCase("Rainha")){
			showPossibleMovesRainha(square);
		}
		
		if(peca.getTypeOfObject().equalsIgnoreCase("Rei")){
			showPossibleMovesRei(square);
		}
		
		if(peca.getTypeOfObject().equalsIgnoreCase("Bispo")) {
			showPossibleMovesBispo(square);
		}
		
		if(peca.getTypeOfObject().equalsIgnoreCase("Torre")) {	
			showPossibleMovesTorre(square);				
		}
	
		if(peca.getTypeOfObject().equalsIgnoreCase("Cavalo")) {	
			showPossibleMovesCavalo(square);
		}
	
	}
}
