package model;

import java.util.ArrayList;
import java.awt.Point;

public class Bispo extends Peca{

	public Bispo(String imagePath){
		super(imagePath);
	}

	public Bispo(String imagePath, String typeOfObject){
		super(imagePath, typeOfObject);
	}

	public ArrayList<Point> getMove(int x, int y){
		
		ArrayList<Point> moveBispo = new ArrayList<>();
		
		for(int i = x, j = y;(i <7 || j <7) ; i++, j++){					

			if(i == 7 || j == 7) {
				break;
			}
			
			Point pointBD = new Point(i+1, j+1);
			moveBispo.add(pointBD);
			
		}

		for(int i = x, j = y; i< 7 || j>0; i++, j--) {

			if(i == 7 || j == 0) {
				break;
			}
			
			Point pointBE = new Point(i+1, j-1);
			moveBispo.add(pointBE);
			
		}

		for(int i = x, j = y; i>0 || j<7; i--, j++){

			if(i == 0 || j == 7) {
				break;
			}

			Point pointCD = new Point(i-1, j+1);
			moveBispo.add(pointCD);
		
		}
		for(int i = x, j = y; i>0 || j>0; i--, j--){

			if(i == 0 || j == 0) {
				break;
			}

			Point pointCD = new Point(i-1, j-1);
			moveBispo.add(pointCD);
			
		}
	
		return moveBispo;
	}
}