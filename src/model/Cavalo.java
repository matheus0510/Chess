package model;

import java.awt.Point;
import java.util.ArrayList;

public class Cavalo extends Peca{
	
	public Cavalo(String imagePath){
		super(imagePath);
	}
	
	public Cavalo(String imagePath, String typeOfObject){
		super(imagePath, typeOfObject);
	}

	public ArrayList<Point> getMove(int x, int y){
		ArrayList<Point> moveCavalo = new ArrayList<>();	

		if(!(y == 6 || y ==7)){ 		
			if(!(x == 0)){
				Point pontoDireitaCima = new Point(x-1, y+2);
				moveCavalo.add(pontoDireitaCima);
			}		
			if(!(x == 7)){
				Point pontoDireitaBaixo = new Point(x+1, y+2);
				moveCavalo.add(pontoDireitaBaixo);
			}			
		}
	
		if(!(y == 0 || y == 1)){
			if(!(x == 0)) {
				Point pontoEsquerdaCima = new Point(x-1, y-2);
				moveCavalo.add(pontoEsquerdaCima);
			}
			if(!(x == 7)){
				Point pontoEsquerdaBaixo = new Point(x+1, y-2);
				moveCavalo.add(pontoEsquerdaBaixo);
			}
		}

		if(!(x == 0 || x == 1)){
			if(!(y == 7)){
				Point pontoCimaDireita = new Point(x-2, y+1);
				moveCavalo.add(pontoCimaDireita);
			}
			if(!(y == 0)){
				Point pontoCimaEsquerda = new Point(x-2, y-1);
				moveCavalo.add(pontoCimaEsquerda);
			}
		}
		
		if(!(x == 7 || x == 6)){	
			if(!(y == 7)){
				Point pontoBaixoDireita = new Point(x+2, y+1);	
				moveCavalo.add(pontoBaixoDireita);
			}
			if(!(y == 0)){
				Point pontoBaixoEsquerda = new Point(x+2, y-1);
				moveCavalo.add(pontoBaixoEsquerda);
			}
		}	
		
		return moveCavalo;
	}
}