package model;

import java.util.ArrayList;
import java.awt.Point;

public class Rainha extends Peca{
	
	public Rainha(String imagePath){
		super(imagePath);
	}

	public Rainha(String imagePath, String typeOfObject){
		super(imagePath, typeOfObject);
	}

	public ArrayList<Point> getMove(int x, int y){
		ArrayList<Point> moveRainha = new ArrayList<>();
		
		for(int i = y+1; i<8; i++){
			Point pontoDireita = new Point(x, i);
			moveRainha.add(pontoDireita);
		}
		for(int i = y-1; i>=0; i--){
			Point pontoEsquerda = new Point(x, i);
			moveRainha.add(pontoEsquerda);
		}
		
		for(int i = x+1; i<8; i++){
			Point pontoCima = new Point(i, y);
			moveRainha.add(pontoCima);
		}
		for(int i = x-1; i>=0; i--){
			Point pontoBaixo = new Point(i, y);
			moveRainha.add(pontoBaixo);
		}
		for(int i = x, j = y;(i <7 || j <7) ; i++, j++){					

			if(i == 7 || j == 7) {
				break;
			}
			
			Point pontoBaixoDireita = new Point(i+1, j+1);
			moveRainha.add(pontoBaixoDireita);
			
		}

		for(int i = x, j = y; i< 7 || j>0; i++, j--){

			if(i == 7 || j == 0) {
				break;
			}
			
			Point pontoBaixoEsquerda = new Point(i+1, j-1);
			moveRainha.add(pontoBaixoEsquerda);
			
		}

		for(int i = x, j = y; i>0 || j<7; i--, j++){

			if(i == 0 || j == 7){
				break;
			}

			Point pontoCimaDireita = new Point(i-1, j+1);
			moveRainha.add(pontoCimaDireita);
		
		}
		for(int i = x, j = y; i>0 || j>0; i--, j--){

			if(i == 0 || j == 0){
				break;
			}

			Point pontoCimaDireita = new Point(i-1, j-1);
			moveRainha.add(pontoCimaDireita);
			
		}
		
		return moveRainha;
	}
}