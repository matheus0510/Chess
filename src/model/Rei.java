package model;

import java.util.ArrayList;
import java.awt.Point;

public class Rei extends Peca{

	public Rei(String imagePath, String typeOfObject){
		super(imagePath, typeOfObject);
	}

	public Rei(String imagePath){
		super(imagePath);
	}

	public ArrayList<Point> getMove(int x, int y){
		ArrayList<Point> moveRei = new ArrayList<>();
		if(!(x == 0)){
			Point pontoCima = new Point(x-1, y);
			moveRei.add(pontoCima);
			if(!(y == 7)){
				Point pontoCimaDireita = new Point(x-1, y+1);
				moveRei.add(pontoCimaDireita);
			}
			if(!(y == 0)){
				Point pontoCimaEsquerda = new Point(x-1, y-1);		
				moveRei.add(pontoCimaEsquerda);
			}
		}
		if(!(x == 7)){
			Point pontoBaixo = new Point(x+1, y);
			moveRei.add(pontoBaixo);
			if(!(y == 7)){
				Point pointBaixoDireita = new Point(x+1, y+1);
				moveRei.add(pointBaixoDireita);
			}
			if(!(y == 0)){
				Point pontoBaixoEsquerda = new Point(x+1, y-1);
				moveRei.add(pontoBaixoEsquerda);
			}
		}
		if(!(y == 7)){		
			Point pontoDireita = new Point(x, y+1);
			moveRei.add(pontoDireita);
		}
		if(!(y == 0)){		
			Point pontoEsquerda = new Point(x,y-1);
			moveRei.add(pontoEsquerda);
		}
		return moveRei;
	}
}