package model;

import java.awt.Point;
import java.util.ArrayList;

public class Peao extends Peca{

	public Peao(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}

	public Peao(String imagePath) {
		super(imagePath);
	}
	
	public ArrayList<Point> getMoveB(int x, int y) {
		ArrayList<Point> movePeao = new ArrayList<>();
		
		if(x == 1) {
			Point pointF = new Point(x + 1, y);
			movePeao.add(pointF);
			
			Point pointFF = new Point(x + 2 , y);
			movePeao.add(pointFF);
			
			Point pointEsquerdo = new Point(x+1, y+1);
			movePeao.add(pointEsquerdo);
			
			Point pointDireito = new Point(x+1, y-1);
			movePeao.add(pointDireito);
		}
		else {
			Point pointEsquerdo = new Point(x+1, y+1);
			movePeao.add(pointEsquerdo);
			
			Point pointDireito = new Point(x+1, y-1);
			movePeao.add(pointDireito);
			
			Point pointFrente = new Point(x+1, y);
			movePeao.add(pointFrente);
		}
		
		return movePeao;
	}
	public ArrayList<Point> getMoveW(int x, int y) {
		
		ArrayList<Point> movePeao = new ArrayList<>();
		
		if(x == 6) {
			Point pointF = new Point(x - 1, y);
			movePeao.add(pointF);
			
			Point pointFF = new Point(x - 2 , y);
			movePeao.add(pointFF);
			
			Point pointEsquerdo = new Point(x-1, y-1);
			movePeao.add(pointEsquerdo);
			
			Point pointDireito = new Point(x-1, y+1);
			movePeao.add(pointDireito);
		}
		else {
			Point pointEsquerdo = new Point(x-1, y-1);
			movePeao.add(pointEsquerdo);
			
			Point pointDireito = new Point(x-1, y+1);
			movePeao.add(pointDireito);
			
			Point pointFrente = new Point(x-1, y);
			movePeao.add(pointFrente);
		}
		
		return movePeao;
	}
}