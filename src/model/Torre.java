package model;

import java.util.ArrayList;
import java.awt.Point;

public class Torre extends Peca{

	public Torre(String imagePath) {
		super(imagePath);
	}

	public Torre(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}

	public ArrayList<Point> getMove(int x, int y) {
		
		ArrayList<Point> moveTorre = new ArrayList<>();
		
		for(int i = y+1; i<8; i++){
			Point pointD = new Point(x, i);
			moveTorre.add(pointD);
		}
		for(int i = y-1; i>=0; i--){
			Point pointE = new Point(x, i);
			moveTorre.add(pointE);
		}
		
		for(int i = x+1; i<8; i++){
			Point pointC = new Point(i, y);
			moveTorre.add(pointC);
		}
		for(int i = x-1; i>=0; i--){
			Point poinB = new Point(i, y);
			moveTorre.add(poinB);
		}
		return moveTorre;
	}
}